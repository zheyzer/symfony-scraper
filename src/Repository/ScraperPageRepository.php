<?php

namespace App\Repository;

use App\Entity\ScraperPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ScraperPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScraperPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScraperPage[]    findAll()
 * @method ScraperPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScraperPageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ScraperPage::class);
    }

    // /**
    //  * @return ScraperPage[] Returns an array of ScraperPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScraperPage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
