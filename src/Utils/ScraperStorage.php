<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 25.04.19
 * Time: 0:46
 */

namespace App\Utils;

use App\Entity\Scraper;
use App\Entity\ScraperPage;
use Doctrine\ORM\EntityManagerInterface;

class ScraperStorage implements StorageInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $scraper;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addScraper(array $data)
    {
        $this->scraper = new Scraper();
        $this->scraper->setName(parse_url($data['url'])['host']);
        $this->scraper->setUrl($data['url']);
    }

    public function addPage(array $data)
    {
        $scraperPage = new ScraperPage();
        $scraperPage->setUrl($data['link']);
        $scraperPage->setImages($data['images']);
        $scraperPage->setTime($data['timer']);
        $this->scraper->addPage($scraperPage);
    }

    public function save()
    {
        $this->entityManager->persist($this->scraper);
        $this->entityManager->flush();
    }

}