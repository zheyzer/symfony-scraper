<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.04.19
 * Time: 12:30
 */

namespace App\Utils;

use Goutte\Client;

class Crawler implements CrawlerInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Crawler
     */
    private $crawler;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getPage(?string $url)
    {
        $this->crawler = $this->client->request('GET', $url);
        return $this->crawler;
    }

    public function getLinks()
    {
        return $this->crawler->filter('a')->extract('href');
    }

    public function getImages()
    {
        return $this->crawler->filter('img')->count();
    }

}