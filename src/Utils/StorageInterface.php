<?php

namespace App\Utils;

interface StorageInterface {
    public function addScraper(array $data);

    public function addPage(array $data);

    public function save();
}