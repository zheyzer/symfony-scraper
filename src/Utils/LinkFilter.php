<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 23.04.19
 * Time: 22:00
 */

namespace App\Utils;


use phpDocumentor\Reflection\Types\Null_;

class LinkFilter implements FilterInterface
{

    /**
     * @var
     */
    private $depth;
    /**
     * @var
     */
    private $domain;

    /**
     * @var
     */
    private $schema;

    public function __construct($url='', $depth=Null)
    {
        $this->depth = $depth;
        $arrSite = parse_url($url);
        $this->schema = $arrSite['scheme'];
        $this->domain = $arrSite['host'];
    }

    public function filter(array $links)
    {
        $arrLinks = [];
        foreach($links as $link) {
            //first step is to validate
            if($arrLink = $this->validate($link)) {
                $arrLinks[] = $this->prepare($arrLink);
            }
        }
        return $arrLinks;
    }

    private function validate($link)
    {
        if(strlen($link)>255) {
            return false;
        }

        $arrUrl = parse_url($link);
        if(array_key_exists('scheme', $arrUrl) && !in_array($arrUrl['scheme'], array('http','https'))) {
            return false;
        }

        if ((array_key_exists('scheme', $arrUrl)) || array_key_exists('path', $arrUrl)) {
            if(array_key_exists('path', $arrUrl)) {
                if($this->depth!==Null) {
                    if (count(preg_split('@/@', $arrUrl['path'], NULL, PREG_SPLIT_NO_EMPTY)) > $this->depth) {
                        return false;
                    }
                }
                if(preg_match('/\.(jpg|png|jpeg|gif)$/', $arrUrl['path'])){
                    return false;
                }
            }
            //check domain id schema

            if (array_key_exists('scheme', $arrUrl)) {
                if ($arrUrl['host'] == $this->domain) {
                    return $arrUrl;
                } else {
                    return false;
                }
            } else {
                $arrUrl['host'] = $this->domain;
                $arrUrl['scheme'] = $this->schema;
                return $arrUrl;
            }
        } else {
            return false;
        }
    }


    private function prepare($arrUrl) {
        $url = sprintf("%s/%s%s", $arrUrl["host"], ((isset($arrUrl["path"])) ? $arrUrl["path"] : ''), ((isset($arrUrl["query"])) ? '?'.$arrUrl["query"] : ''));
        $url = $arrUrl["scheme"] . '://' . str_replace('//','/', $url);
        return $url;
    }
}