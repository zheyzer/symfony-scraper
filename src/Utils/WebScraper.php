<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 21.04.19
 * Time: 23:59
 */

namespace App\Utils;

class WebScraper
{
    /**
     * @var int
     */
    private $countPages;
    /**
     * @var string
     */
    private $url;
    /**
     * @var Array
     */
    private $links;

    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * @var FilterInterface
     */
    private $filter;

    /**
     * @var integer
     */
    private $depth;

    public function __construct(ScraperBuilder $builder)
    {
        $this->url = $builder->getUrl();
        $this->countPages = $builder->getCountPages();
        $this->depth = $builder->getDepth();
        $this->crawler = $builder->getCrawler();
        $this->filter = $builder->getFilter();
        $this->storage = $builder->getStorage();
    }

    public function execute() {
        $scraperData = [
            'url' => $this->url
        ];

        $this->storage->addScraper($scraperData);
        $this->links[] = $this->url;

        /* crawler started here*/
        $i = 0;
        while(isset($this->links[$i])) {
            $link = $this->links[$i];
            //get page and parse them
            $start = microtime(true);
            $this->crawler->getPage($link);
            $this->links = array_unique(array_merge($this->links, $this->filter->filter($this->crawler->getLinks())));
            $images = $this->crawler->getImages();
            $timer = microtime(true) - $start;
            $pageData = [
                'link' => $link,
                'images' => $images,
                'timer' => $timer,
            ];
            $this->storage->addPage($pageData);
            $i++;
            if($this->countPages && $i>=$this->countPages) {
                break;
            }
        }

        $this->storage->save();
    }
}