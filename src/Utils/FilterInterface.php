<?php

namespace App\Utils;

interface FilterInterface
{
    public function filter(array $data);
}