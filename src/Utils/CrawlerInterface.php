<?php

namespace App\Utils;

interface CrawlerInterface
{
    public function getPage(?string $url);

    public function getLinks();

    public function getImages();
}