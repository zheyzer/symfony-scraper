<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScraperRepository")
 */
class Scraper
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ScraperPage", mappedBy="scraper", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"images" = "DESC"})
     */
    private $pages;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection|ScraperPage[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(ScraperPage $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setScraper($this);
        }

        return $this;
    }

    public function removePage(ScraperPage $page): self
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getScraper() === $this) {
                $page->setScraper(null);
            }
        }

        return $this;
    }
}
