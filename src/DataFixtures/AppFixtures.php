<?php

namespace App\DataFixtures;

use App\Entity\Scraper;
use App\Entity\ScraperPage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const SCRAPER = [
        [
            'name' => 'Scraper 1',
            'url' => 'https://scraper.com'
        ],
        [
            'name' => 'Scraper 2',
            'url' => 'https://parser.com'
        ]
    ];

    const PAGES = [
        [
            'url' => '/page1',
            'images' => 20,
            'time' => 35
        ],
        [
            'url' => '/page2',
            'images' => 30,
            'time' => 45
        ],
        [
            'url' => '/page3',
            'images' => 10,
            'time' => 25
        ],[
            'url' => '/page4',
            'images' => 100,
            'time' => 100
        ],
        [
            'url' => '/page5',
            'images' => 15,
            'time' => 30
        ],
        [
            'url' => '/page6',
            'images' => 17,
            'time' => 25
        ],
        [
            'url' => '/page7',
            'images' => 35,
            'time' => 49
        ],
        [
            'url' => '/page8',
            'images' => 37,
            'time' => 47
        ],
        [
            'url' => '/page9',
            'images' => 15,
            'time' => 30
        ]
        ,[
            'url' => '/page10',
            'images' => 150,
            'time' => 300
        ],
        [
            'url' => '/page11',
            'images' => 12,
            'time' => 25
        ]

    ];

    public function load(ObjectManager $manager)
    {
        $this->loadScraper($manager);
        $this->loadPages($manager);
    }

    public function loadScraper(ObjectManager $manager) {
        foreach(self::SCRAPER as $item) {
            $scraper = new Scraper();
            $scraper->setName($item['name']);
            $scraper->setUrl($item['url']);
            $this->addReference($item['url'], $scraper);
            $manager->persist($scraper);
        }

        $manager->flush();
    }

    public function loadPages(ObjectManager $manager) {

        foreach(self::PAGES as $page) {
            $scraperPage = new ScraperPage();
            $domain = self::SCRAPER[rand(0,1)]['url'];
            $scraperPage->setUrl($domain . $page['url']);
            $scraperPage->setImages($page['images']);
            $scraperPage->setTime($page['time']);
            $scraperPage->setScraper($this->getReference($domain));
            $manager->persist($scraperPage);
        }

        $manager->flush();
    }
}
