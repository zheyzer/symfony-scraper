<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.04.19
 * Time: 10:48
 */

namespace App\Command;


use App\Utils\Crawler;
use App\Utils\LinkFilter;
use App\Utils\ScraperBuilder;
use App\Utils\StorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Goutte\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScraperCommand extends Command
{

    /**
     * @var ScraperBuilder
     */
    private $scraperBuilder;
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var EntityManagerInterface
     */

    public function __construct($name = null, ScraperBuilder $scraperBuilder, StorageInterface $storage)
    {
        parent::__construct($name);
        $this->scraperBuilder = $scraperBuilder;
        $this->storage = $storage;
    }

    protected function configure()
    {
        $this->setName('scraper:execute')
            ->setDescription('Image Scraper')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'Enter url for scrap'
            )
            ->addArgument(
                'count_pages',
                InputArgument::OPTIONAL,
                'Enter count pages'
            )
            ->addArgument(
                'depth',
                InputArgument::OPTIONAL,
                'Enter depth link'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
        $count_pages = $input->getArgument('count_pages');
        $depth = $input->getArgument('depth');

        $crawler = new Crawler(new Client());
        $linkFilter = new LinkFilter($url, $depth);
        $scraper = $this->scraperBuilder->setUrl($url)
            ->setCrawler($crawler)
            ->setFilter($linkFilter)
            ->setStorage($this->storage)
            ->setCountPages($count_pages)
            ->build();
        $scraper->execute();

        $output->writeln('Scraping site '.  $url . ' was ended.');

//        if($input->getArgument('depth')) {
//            $output->writeln('Depth links: ' . $input->getArgument('depth'));
//        }
    }

}