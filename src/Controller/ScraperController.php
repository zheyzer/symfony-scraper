<?php

namespace App\Controller;

use App\Entity\Scraper;
use App\Form\ScraperType;
use App\Repository\ScraperRepository;
use App\Utils\Crawler;
use App\Utils\LinkFilter;
use App\Utils\ScraperBuilder;
use App\Utils\StorageInterface;
use App\Utils\WebScraper;
use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ScraperController extends AbstractController
{

    /**
     * @var ScraperRepository
     */
    private $scraperRepository;

    public function __construct(ScraperRepository $scraperRepository)
    {
        $this->scraperRepository = $scraperRepository;
    }

    /**
     * @Route("/scraper", name="scraper")
     */
    public function index()
    {
        $scrapers = $this->scraperRepository->findAll();
       // var_dump($scrapers);die();

        return $this->render('scraper/index.html.twig', [
            'controller_name' => 'ScraperController',
            'scrapers' => $scrapers
        ]);
    }

    /**
     * @Route("/scraper/add", name="scraper_add")
     */
    public function add(Request $request, ScraperBuilder $scraperBuilder, StorageInterface $storage) {

        $form = $this->createForm(ScraperType::class);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $data = $form->getData();
            $crawler = new Crawler(new Client());
            $linkFilter = new LinkFilter($data['url'], $data['depth']);
            $scraper = $scraperBuilder->setUrl($data['url'])
                ->setCrawler($crawler)
                ->setFilter($linkFilter)
                ->setStorage($storage)
                ->setCountPages($data['count_pages'])
                ->build();
            $scraper->execute();

            $this->addFlash('notice', 'Scraping ' . $data['url'] . ' was finished');

            return $this->redirectToRoute('scraper');
        }

        return $this->render('scraper/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/scraper/{id}", name="scraper_detail")
     */
    public function detail(Scraper $scraper)
    {
        return $this->render('scraper/detail.html.twig',[
            'scraper' => $scraper
        ]);
    }

    /**
     * @Route("/scraper/delete/{id}", name="scraper_delete")
     */
    public function delete(Scraper $scraper)
    {
        $em =$this->getDoctrine()->getManager();
        $em->remove($scraper);
        $em->flush();


        $this->addFlash('notice', 'Scraper with info was deleted');

        return $this->redirectToRoute('scraper');
    }
}
