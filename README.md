# Symfony image scraper

It is a simple project with scraper pages and count images on Symfony.

## Installation

This project use docker that makes deply easy. Just do steps under list

```bash
docker-compose up -d
```
Waiting while docker build and run containers

Than you have to install dependies and run migrations and fixtures(optional - test data). Comand list is uder. All comands will be run from container(php-fpm)
```bash
docker exec -it scraper-php-fpm composer install
docker exec -it scraper-php-fpm bin/console doctrine:migrations:migrate
```
If you do all from list project is run.
Open page http://loaclhost:8000(docker must be run at this momemt)

## Test Data
This step is not necessary
You can install test data to database if you wanted. But you need to do two steps:
1. Install doctrine fixtures bundle
2. run fixtues
```bash
composer require --dev doctrine/doctrine-fixtures-bundle
docker exec -it scraper-php-fpm bin/console doctrine:fixtures:load
```

## Usage
Scraper can be used from console
It has three parameters. First is required - url and 2 optional count pages and depth(max link depth). Example near
```bash
docker exec -it scraper-php-fpm bin/console site_url count_pages depth
```
where site_url - its site with host and scheme

If all list is done all have to work without problem.

## License
[MIT](https://choosealicense.com/licenses/mit/)
